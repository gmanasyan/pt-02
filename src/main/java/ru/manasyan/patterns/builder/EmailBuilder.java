package ru.manasyan.patterns.builder;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

public class EmailBuilder {

    private static String DEFAULT_EMAIL = "defaultemail@gmail.com";

    String subject;
    String from = DEFAULT_EMAIL;
    Set<String> emailsTo = new HashSet<>();
    Set<String> emailsCopy = new HashSet<>();
    Content content;

    public EmailToBuilder subject(String emailSubjects) {
        subject = emailSubjects;
        return new EmailToBuilder();
    }

    public class EmailToBuilder {

        public EmailToBuilder from(String emailFrom) {
            from = emailFrom;
            return this;
        }

        public EmailToMultipleBuilder to(String email) {
            emailsTo.add(email);
            return new EmailToMultipleBuilder();
        }

        public EmailToMultipleBuilder toAll(Collection allEmails) {
            emailsTo.addAll(allEmails);
            return new EmailToMultipleBuilder();
        }

    }

    public class EmailToMultipleBuilder {

        public EmailToMultipleBuilder from(String emailFrom) {
            from = emailFrom;
            return this;
        }

        public EmailToMultipleBuilder to(String email) {
            emailsTo.add(email);
            return new EmailToMultipleBuilder();
        }

        public EmailToMultipleBuilder toAll(Collection allEmails) {
            emailsTo.addAll(allEmails);
            return new EmailToMultipleBuilder();
        }

        public EmailCopyBuilder copyTo(String email) {
            return (new EmailCopyBuilder()).copyTo(email);
        }

        public EmailCopyBuilder copyToAll(Collection allCopyEmails) {
            return (new EmailCopyBuilder()).copyToAll(allCopyEmails);
        }

    }

    public class EmailCopyBuilder {

        public EmailCopyBuilder from(String emailFrom) {
            from = emailFrom;
            return this;
        }

        public EmailCopyBuilder copyTo(String email) {
            if (!emailsTo.contains(email)) {
                emailsCopy.add(email);
            }
            return this;
        }

        public EmailCopyBuilder copyToAll(Collection allCopyEmails) {
            allCopyEmails.removeAll(emailsTo.stream().collect(Collectors.toList()));
            emailsCopy.addAll(allCopyEmails);
            return this;
        }

        public GetBuilder content(String body, String signature) {
            return (new ContentBuilder())
                    .content(new Content(body, signature));
        }

        public GetBuilder content(String body) {
            return (new ContentBuilder())
                    .content(new Content(body));
        }

    }

    public class ContentBuilder {

        public ContentBuilder from(String emailFrom) {
            from = emailFrom;
            return this;
        }

        public GetBuilder content(Content emailContent) {
            content = emailContent;
            return new GetBuilder();
        }
    }

    public class GetBuilder {

        public GetBuilder from(String emailFrom) {
            from = emailFrom;
            return this;
        }

        public Email build() {
            return new Email(subject, from, emailsTo, emailsCopy, content);
        }
    }
}
