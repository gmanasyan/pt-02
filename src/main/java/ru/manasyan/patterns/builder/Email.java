package ru.manasyan.patterns.builder;

import java.util.HashSet;
import java.util.Set;

public class Email {
    private String subject;
    private String from;
    private Set<String> emailsTo = new HashSet<>();
    private Set<String> emailsCopy = new HashSet<>();
    private Content content;

    public Email(String subject, String from, Set<String> emailsTo,
                 Set<String> emailsCopy, Content content) {
        this.subject = subject;
        this.from = from;
        this.emailsTo = emailsTo;
        this.emailsCopy = emailsCopy;
        this.content = content;
    }

    public String getSubject() {
        return subject;
    }

    public String getFrom() {
        return from;
    }

    public Set<String> getEmailsTo() {
        return emailsTo;
    }

    public Set<String> getEmailsCopy() {
        return emailsCopy;
    }

    public Content getContent() {
        return content;
    }
}
