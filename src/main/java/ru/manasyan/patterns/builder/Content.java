package ru.manasyan.patterns.builder;

public class Content {
    private static String DEFAULT_SIGNATURE = "Best regards, Georgy.";

    private String body;
    private String signature = DEFAULT_SIGNATURE;

    public Content(String body, String signature) {
        this.body = body;
        this.signature = signature;
    }

    public Content(String body) {
        this.body = body;
    }

    public String getBody() {
        return body;
    }

    public String getSignature() {
        return signature;
    }

    @Override
    public String toString() {
        return "Content{" +
                "body='" + body + '\'' +
                ", signature='" + signature + '\'' +
                '}';
    }
}
