package ru.manasyan.patterns.builder;

import java.util.ArrayList;
import java.util.Arrays;

public class Main {

    public static void main(String[] args) {

        Email email1 = new EmailBuilder()
                .subject("Happy New Year 2021")
                .to("email1@ya.ru")
                .from("from@gmail.com")
                .copyTo("emailcopy2@ya.ru")
                .content("Hello there!", "--- Have a nice day!")
                .build();

        Email email2 = new EmailBuilder()
                .subject("Welcome to Google!")
                .to("email1@ya.ru")
                .to("email2@ya.ru")
                .toAll(Arrays.asList("email2@ya.ru", "email3@ya.ru"))
                .toAll(Arrays.asList("email4@ya.ru", "email5@ya.ru"))
                .from("new_mail@gmail.com")
                .copyTo("email2@ya.ru")
                .copyTo("copy_email@ya.ru")
                .copyToAll(new ArrayList<>(Arrays.asList("email4@ya.ru", "email5@ya.ru")))
                .copyToAll(new ArrayList<>(Arrays.asList("email6@ya.ru")))
                .content("Welcome to new company!")
                .build();

        printEmail(email1);
        printEmail(email2);
    }

    private static void printEmail(Email email) {
        System.out.println("===== Email ===== ");
        System.out.println("From : " + email.getFrom());
        System.out.println("To : " + email.getEmailsTo());
        System.out.println("CopyTo : " + email.getEmailsCopy());
        System.out.println("Subject: " + email.getSubject());
        System.out.println("Content: " + email.getContent());
        System.out.println();
    }


}
